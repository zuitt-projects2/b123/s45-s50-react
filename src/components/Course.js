import React,{useState,useEffect} from 'react'
import {Card,Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Course ({courseProp}) {

	console.log(courseProp)

//States in reactjs are ways to store information within a component. The advandtage of a state from a variable is that variable do not retain updated information when the component is updated

/*const [count,setCount] = useState(0)
const [seats,setSeats] = useState(10)
const [isActive,setIsActive] = useState(true)*/
//useState is a react hook which allows us to create a state and its setter function. useState actually returns 2 items in an array,

//this console.log repeats whenever the enroll buttom is pressed because whenever a state is updated, the component re-renders.
//rendering in reactjs, rendering is the act of showing our elements in the component. It is the run of our component

//console.log(courseProp)


/*let varCount = 0*/

/*useEffect(()=>{
	if(seats === 0){
		setIsActive(false)
	}
},[seats])
console.log(isActive);


function enroll(){
/*	alert("Hello!")
	setCount(count +1)
	setSeats(seats -1)*/


	return (

			<Card>
				<Card.Body>
					<Card.Title>{courseProp.name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{courseProp.description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{courseProp.price}</Card.Text>
					<Link className="btn btn-primary" to={`/courses/${courseProp._id}`}>View Course</Link>
				</Card.Body>
			</Card>
			)
}