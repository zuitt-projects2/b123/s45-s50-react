import React from 'react'
import {Row,Col,Card} from 'react-bootstrap'

export default  function Highlights (){

	return(

			<Row>
				<Col xs={12} md={4}>
					<Card className = "cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Learn From Home</h2>
							</Card.Title>
							<Card.Text>
							 	Ut officia in cupidatat aliquip quis qui dolor sed labore culpa in in proident nulla non aliquip ut excepteur. Ea ut ut mollit.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className = "cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Study Now Pay Later</h2>
							</Card.Title>
							<Card.Text>
							 	Ut officia in cupidatat aliquip quis qui dolor sed labore culpa in in proident nulla non aliquip ut excepteur. Fugiat ea ullamco nulla officia veniam proident nisi in in quis quis mollit laboris duis culpa pariatur deserunt occaecat.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className = "cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
							 	Ut officia in cupidatat aliquip quis qui dolor sed labore culpa in in proident nulla non aliquip ut excepteur. Adipisicing deserunt amet in incididunt ut ad reprehenderit adipisicing adipisicing nostrud culpa aliqua incididunt do nulla aliqua excepteur nulla incididunt amet labore sunt dolor adipisicing sed magna ea irure ad voluptate elit.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>

		)
}