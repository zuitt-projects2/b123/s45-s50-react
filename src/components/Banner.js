import React from 'react';
import {Row,Col,Button,Jumbotron} from 'react-bootstrap'
import {Link} from 'react-router-dom'

/*
Row and Col are components from our react-bootstrap module
They create div elements with bootstrap classes.

react-bootstrap component create react elements with bootstrap classes.

*/

export default function Banner ({bannerProp}) {


	return (

			<Row>
				<Col>
					<Jumbotron>
					<h1>{bannerProp.title}</h1>
					<p>{bannerProp.description}</p>
					<Link to ={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonCallToAction}</Link>
					</Jumbotron>
				</Col>
			</Row>


		)
}