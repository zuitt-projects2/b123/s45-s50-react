import React from 'react'
import Banner from '../components/Banner'

export default function NotFound({bannerProp}){

	let bannerContent = { 
		title:"Page Not Found",
		description:"Book your favorite Course",
		buttonCallToAction: "back to home",
		destination:"/"
	}
	return(
			
				<Banner bannerProp ={bannerContent} />
			
		)


}