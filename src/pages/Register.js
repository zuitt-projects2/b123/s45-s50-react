import React,{useState,useEffect,useContext} from 'react'
import {Form,Button} from 'react-bootstrap'
//import sweetalert2
import Swal from 'sweetalert2'

import UserContext from '../userContext'

import {Redirect,useHistory} from 'react-router-dom'

export default function Register(){

	const{user} = useContext(UserContext)

	const history = useHistory();

	/*
		Review

			Props - Props is a way to pass data from a parent component to a child component. it is used like and HTML attribute added to the child component. The prop the nbecomes a property of the special react that all components receive, the props. Prop names are user-defined.

		States

			States are a way to store information within a component. This information can be updated within the component. When a state is updated through its setter function, it will re-render the component. States are contained within their components. They are independent from other instances of the component.

		Hooks
			Special/react-defined methods and functions that allow us to do certain tasks in our components.

		useState()
			useState() is a hook that create states. useStates() returns an array with 2 items. The first item in the array is state and the second one is setter function. We then destructure this returned array and assign both items in variables:

			const[stateName,setStateName]=useState(initial value of State)

		useEffect()
			useEffect() is a hook used to create effects. These effects will allow us to run a function or task based on when our effect will run. Our useEffect wil ALWAYS run on  initial render. The next time that the useEffect will run will depend on its dependency array.

			useEffect(()=>{},[dependency array]) - (dependency array is optional)

			-useEffect will allows to run a function or task on initial render and whenever our component re-renders IF there is no dependency array

			therefore useEffect will allow us to run a function or task on initial render ONLY if there is an EMPTY dependency array

			-useEffect will allow us to run a function or task on initial render AND whenever the state/s in its dependency array is updated.

			-useEffect will always run on initial render or the very first time our component is displated or run.
	*/
	//input states
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")

	//conditional rendering for button
	const [isActive,setIsActive] = useState(false)

	/*
		Two Way Binding

		In Reactjs, we are able to create forms which will allow us to bind the value of our input as the value for our states. We cannot type into our inputs anymore because there is now a value bond to it. We will then add an onChange event per input to be able to update the state with the current value of the input.

		Two Way Binding is done so that we can ensure that we can save the value of our input in our states. So that we can capture the current value of the input as it is typed on and save in a state as opposed to saving it when we are about to submit the values.

		syntax : <Form.Control type="inputType" value={inputState} onChange={e=>{setInputState(e.target.value)}}/>

		e = event, all event listeners pass the event object to the function added in the event listener

		e.target = target is the element WHERE the event happened.

		e.target.value = value is a property of target. It is the current value of the element WHERE the event happened

	*/


	//Validate our form as our user is typing in and disable the button while at least one of the fields are empty, or when the mobileNo is less than 11 digits and if the password doesn't match

	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (mobileNo.length ===11)&&(password === confirmPassword)){

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	function registerUser(e){

		//prevents submit event's default behavior
		e.preventDefault()
			console.log(firstName)
			console.log(lastName)
			console.log(email)
			console.log(mobileNo)
			console.log(password)
			console.log(confirmPassword)

			fetch('http://localhost:4000/users/',{

				method :'POST',
				headers:{
					"Content-Type":"application/json"
				},
				body:JSON.stringify({
					firstName: firstName,
					lastName:lastName,
					email:email,
					mobileNo:mobileNo,
					password:password
				})
			})
			.then(res=>res.json())
			.then(data =>{
				console.log(data)
				//email property will only be not undefined if we registered properly
				if(data.email){
					Swal.fire({

						icon:"success",
						title:"Registration Successful!",
						text:`Thank you for registering, ${data.email}`
					})
					//redirect our user after registering to our login page.
					history.push('/login')
				} else {
					Swal.fire({
						icon:"error",
						title:"Registration Failed",
						text: data.message
					})
				}
			})
	}

	return (
		user.id
		?
		<Redirect to="/courses"/>
		:
		<>
			<h1 className ="my-5 text-center">Register</h1>
			<Form onSubmit = {e=>registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name: </Form.Label>
					<Form.Control type= "text" value ={firstName} onChange={e => {setFirstName(e.target.value)}} placeholder = "Enter First Name" required/>
				</Form.Group> 
				<Form.Group>
					<Form.Label>Last Name: </Form.Label>
					<Form.Control type= "text" value ={lastName} onChange={e => {setLastName(e.target.value)}} placeholder = "Enter Last Name" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email: </Form.Label>
					<Form.Control type= "email" value ={email} onChange={e => {setEmail(e.target.value)}} placeholder = "Enter Email" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No: </Form.Label>
					<Form.Control type= "number" value ={mobileNo} onChange={e => {setMobileNo(e.target.value)}} placeholder = "Enter 11 Digit Mobile No" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password: </Form.Label>
					<Form.Control type= "password" value ={password} onChange={e => {setPassword(e.target.value)}} placeholder = "Enter Password" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password: </Form.Label>
					<Form.Control type= "password" value ={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} placeholder = "Confirm Password" required/>
				</Form.Group> 
				{
					isActive
					?<Button variant="primary" type="submit">Submit</Button>
					:<Button variant="primary" disabled>Submit</Button>
				}
			</Form>
		</>


		)

}