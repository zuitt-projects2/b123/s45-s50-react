import React,{useState,useEffect,useContext} from 'react'
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Redirect,useHistory} from 'react-router-dom'
import UserContext from '../userContext'
export default function AddCourse(){

	const {user} = useContext(UserContext)
	const history = useHistory();
	const [name,setName] = useState("")
	const [description,setDescription] = useState("")
	const [price,setPrice] = useState(0)
	const [isActive,setIsActive] = useState(true)
	function reset(){
		setName("")
		setDescription("")
		setPrice("")
	}

	useEffect(()=>{

		if(name !== "" && description !== "" && price !== 0){

			setIsActive(true)
		} else {

			setIsActive(false)
		}
	},[name, description, price]) 
	
	function addCourse(e){

		e.preventDefault()
		fetch('http://localhost:4000/courses/',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`

			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price,
			})
		})
		.then(res => res.json())
		.then(data => {

			
			if(data.message){
				Swal.fire({

					icon: "error",
					title: "Course Creation Failed.",
					text: data.message
				})
			} else {
				console.log(data)
				Swal.fire({

					icon: "success",
					title: "Course Creation Successful.",
					text: `Course has been created.`
				})
				history.push("/courses")
			}

		})
		reset()	
	}

	
	return (
		user.isAdmin === false 
		?
		<Redirect to="/"/>
		:
		<>
			<h1>Create Course</h1>
			<Form onSubmit={ e => addCourse(e)}>
				<Form.Group controlId="name">
					<Form.Label>Course Name:</Form.Label>
					<Form.Control type= "text" value ={name} onChange={e => {setName(e.target.value)}} placeholder = "Enter Name" required/>
				</Form.Group>
				<Form.Group controlId="description">
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" value={description} onChange={(e) => setDescription(e.target.value)}required/>
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Label>Price:</Form.Label>
					<Form.Control type="number" value={price} onChange={(e) => setPrice(e.target.value)}required/>
				</Form.Group>
				{
					isActive 
					? <Button type="submit" variant="primary">Submit</Button>
					: <Button type="submit" variant="primary" disabled>Submit</Button>
				}
			</Form>
		</>
		

		)

}
