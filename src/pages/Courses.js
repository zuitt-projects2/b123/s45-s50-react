import React,{useState,useEffect} from 'react'
//import our mock data
import coursesData from '../data/coursesData'
//import course component
import Course from '../components/Course'


export default function Courses(){

	//create a state with empty array as initial value
	const [coursesArray,setCoursesArray] = useState([])

	useEffect(()=>{

		fetch("http://localhost:4000/courses/getActiveCourses")
		.then(res => res.json())
		.then(data =>{

			//resulting new array from mapping the data (which is an array of course documents) will be set into our courseArray state with
			setCoursesArray(data.map(course => {

					return (
						//In reactjs, when creating a group/array of react element, we need to pass a key prop so that reacjs can recognize each individual of instances of the react element. Each key prop should be unique.
							
								<Course key={course._id} courseProp ={course}/>
						)
				
			}))
		})
	},[])

	



/*
	Each instance of a component is independent from one another.

	So if for example, we called multiple course components, each of those instances are independent from each other. therefore allowing us to have re-usable UI components

*/	
/*	let coursesCards = coursesData

	console.log(coursesCards)*/

	


	return(
			<>
				
				<h1 className="my-5">Available Courses</h1>
				{coursesArray}
			</>
		)	
}