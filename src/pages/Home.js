import React from 'react';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

/*
	Home will be a page component, which will be our pages for our application

	ReactJs adheres to D.R.Y - Don't Repeat Yourself

	Props - are date we can pass from a parent component to child component

	All components actually are able to receive an object, Props are special react objects with which we can pass data around from a parent to child
*/
export default function Home (){

	let sampleProp = {
	title:"Batch 123 Booking System",
	description:"Book your Course",
	buttonCallToAction: "Join Us!",
	destination:"/login"}
	/*
		We can pass props from a parent to child by adding HTML-like attribute which we can name ourselves 
	*/
	return(


			<>
				<Banner bannerProp ={sampleProp}/>
				<Highlights/>
			</>
		)
}