import React,{useContext,useEffect} from 'react'
import UserContext from '../userContext'
import Banner from '../components/Banner'

export default function Logout(){

	//destructure the returned object of useContext after unwrapping our context:
	const {setUser,unsetUser} = useContext(UserContext)

/*	console.log(setUser)
	console.log(unsetUser)*/

	//Clear the localStorage
	unsetUser();

	//add empty dependency array to run the useEffect only on initial render.
	useEffect(()=>{

		//set the global user state to its initial values.
		//Can you update a state included in the context with its setter function? YES.
		setUser({
			id:null,
			isAdmin:null
		})
	},[])

	const bannerContent = {
		title: "See you late!",
		description : " You have logged out of Booking System",
		buttonCallToAction: "Go Back to Home Page.",
		destination:"/"
	}

	return (

			<Banner bannerProp={bannerContent}/>
		)
}