import React from 'react'

let coursesData= [
	{
		id:"wdc001",
		name:"PHP-Laravel",
		description: "Dolore in labore voluptate ut minim ut occaecat dolore mollit labore in non aliquip aliqua minim voluptate cillum.",
		price: 45000,
		onOffer: true
	},
	{
		id:"wdc002",
		name:"Python-Django",
		description: "Dolore in labore voluptate ut minim ut occaecat dolore mollit labore in non aliquip aliqua minim voluptate cillum. Pariatur in nostrud.",
		price: 50000,
		onOffer: true
	},
	{
		id:"wdc003",
		name:"Java-Springboot",
		description: "Dolore in labore voluptate ut minim ut occaecat dolore mollit labore in non aliquip aliqua minim voluptate cillum. Dolor pariatur aute ad dolore qui pariatur cillum ut ut.",
		price: 55000,
		onOffer: true
	},
	{
		id:"wdc004",
		name:"Nodejs-MERN",
		description: "Dolore in labore voluptate ut minim ut occaecat dolore mollit labore in non aliquip aliqua minim voluptate cillum. Consectetur cillum esse reprehenderit irure pariatur eu in qui adipisicing aliquip consectetur culpa ut dolore anim reprehenderit laboris elit sint laboris dolor do nisi et aliqua consequat deserunt qui nulla.",
		price: 45000,
		onOffer: false
	}
]

export default coursesData