//When creating a component in a separate file, We always import: react

import React, {useState,useEffect} from 'react';
import Banner from './components/Banner'
import AppNavBar from './components/AppNavBar'

//import pages
import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import NotFound from './pages/NotFound'
import Logout from './pages/Logout'
import AddCourse from  './pages/AddCourse';
import ViewCourse from  './pages/ViewCourse';
//import react router dom components for simulated page routing:
import {BrowserRouter as Router} from 'react-router-dom';
import{Route,Switch} from 'react-router-dom'

//import our user provider the provide component of our context
import {UserProvider} from './userContext'



import './App.css'

//import container from react-bootstrap;
import {Container} from 'react-bootstrap'

export default function App() {

const [user,setUser] = useState({

	id:null,
	isAdmin:null
})
	
useEffect(()=>{
		fetch('http://localhost:4000/users/getUserDetails',{

			headers:{
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id:data._id,
				isAdmin:data.isAdmin
			})
		})
},[])
console.log(user)
	/*
		ReactJS is a single page application (SPA), however, we can actually simulate the changing of pages. we dont actually create new page, what we just did is switch pages according their assigned routes. ReactJS and react-router-dom package just mimics or mirrors how HTML accesses its urls.

		With this, we dont reload the page each time we switch pages.

		react-router-dom has 3 main components to simulate the changing of pages:

			Router
				Wrapping our Router component around our other components

			Route
				assigns a path which will trigger the change/switch of components to render.
	*/

	//function to clear localStorage on logout
	const unsetUser=()=> {
		localStorage.clear()
	}

	return (
		<>
			<UserProvider value ={{user,setUser,unsetUser}}>
				<Router>
					<AppNavBar/>
					<Container>
						<Switch>
							<Route exact path ="/" component = {Home}/>
							<Route exact path ="/courses" component = {Courses}/>
							<Route exact path ="/courses/:courseId" component = {ViewCourse}/>
							<Route exact path ="/login" component = {Login}/>
							<Route exact path ="/register" component = {Register}/>
							<Route exact path ="/logout" component = {Logout}/>
							<Route exact path="/addCourse" component={AddCourse}/>
							<Route component = {NotFound}/>
						</Switch>
					</Container>
				</Router>
			</UserProvider>
		</>
		)
}